# Tempo

Set a host and start pinging! Your RTT results will be graphed.

![](images/example_1.png)

## Downloading

Check out [tags](https://gitlab.com/brandonp2412/tempo/tags) to download for Windows/Linux. If you want to create a MacOS image, set up this repository locally and run `npm run electron:mac`.

## Getting Started

Clone this repository locally :

```bash
git clone https://gitlab.com/brandonp2412/tempo.git
```

Install dependencies with npm :

```bash
npm install
```

There is an issue with `yarn` and `node_modules` that are only used in electron on the backend when the application is built by the packager. Please use `npm` as dependencies manager.

## Included Commands

| Command                    | Description                                                                                                 |
| -------------------------- | ----------------------------------------------------------------------------------------------------------- |
| `npm run ng:serve:web`     | Execute the app in the browser                                                                              |
| `npm run build`            | Build the app. Your built files are in the /dist folder.                                                    |
| `npm run build:prod`       | Build the app with Angular aot. Your built files are in the /dist folder.                                   |
| `npm run electron:local`   | Builds your application and start electron                                                                  |
| `npm run electron:linux`   | Builds your application and creates an app consumable on linux system                                       |
| `npm run electron:windows` | On a Windows OS, builds your application and creates an app consumable in windows 32/64 bit systems         |
| `npm run electron:mac`     | On a MAC OS, builds your application and generates a `.app` file of your application that can be run on Mac |
