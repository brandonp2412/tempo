export interface Ping {
  when: Date;
  duration: number;
}
