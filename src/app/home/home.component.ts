import { Component } from "@angular/core";
import { ChartData } from "../models/chart-data.model";
import { Subscription, interval } from "rxjs";
import {
  faTrash,
  faPause,
  faPlay,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { ElectronService } from "../providers/electron.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent {
  host = "google.co.nz";
  chartData: ChartData[] = [{ data: [], label: "Duration (ms)" }];
  chartLabels: string[] = [];
  pinging: boolean;
  interval = 1;
  disconnected = false;
  statuses: string[] = [];
  alertSuccess = false;
  alertFailure = false;

  faPlay = faPlay;
  faTrash = faTrash;
  faPause = faPause;
  faCheck = faCheck;

  private pinger: Subscription;

  constructor(private electron: ElectronService) {}

  startPinging() {
    this.pinger = interval(this.interval * 1000).subscribe(() => this.ping());
    this.pinging = true;
  }

  stopPinging() {
    this.pinger.unsubscribe();
    this.pinging = false;
  }

  ping() {
    this.electron.ping(this.host).then(
      ms => this.addPing(ms),
      _stderr => {
        this.disconnected = true;
      }
    );
  }

  addPing(duration: number) {
    this.disconnected = false;
    this.chartData[0].data.push(duration);
    this.chartLabels.push(new Date().toLocaleTimeString());
  }

  clearData() {
    this.chartData[0].data = [];
    this.chartLabels = [];
  }
}
